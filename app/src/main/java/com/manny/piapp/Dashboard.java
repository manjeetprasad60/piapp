package com.manny.piapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class Dashboard extends AppCompatActivity {

    private TextView temperaturevalue , humidityvalue, lightvalue,colorvalue;
    private DatabaseReference databaseReference_sensor, databaseReference_temp,databaseReference_light,databaseReference_color;
    private ProgressBar progressBar;
    private ProgressBar progressBaranimator;
    private ObjectAnimator objectAnimator;
    private LinearLayout linearLayout;

    private TextView textViewnumber;
    private  float value;

    private   int tempvalue;
    private  int progress = 0;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        temperaturevalue = findViewById(R.id.temperature_number);
        humidityvalue = findViewById(R.id.humidity_number);
        lightvalue = findViewById(R.id.ldr_number);
        colorvalue = findViewById(R.id.color_text);
        progressBar = findViewById(R.id.progresbar);
        linearLayout = findViewById(R.id.mainLinear);
        textViewnumber = findViewById(R.id.number);


        progressBar.setVisibility(View.VISIBLE);
        linearLayout.setVisibility(View.GONE);




        databaseReference_sensor = FirebaseDatabase.getInstance().getReference().child("sensor").child("Humidity");

        databaseReference_sensor.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {

                if(snapshot.getValue()==null){
//                    button_createservice.setVisibility(View.VISIBLE);
//                    recyclerView_serviceView.setVisibility(View.GONE);
                }
                else {

                       String humid = snapshot.getValue().toString();
                    progressBar.setVisibility(View.GONE);
                    linearLayout.setVisibility(View.VISIBLE);

                      //  temperaturevalue.setText();
                        humidityvalue.setText(humid+"%");



                    }



                }



            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });


        databaseReference_temp = FirebaseDatabase.getInstance().getReference().child("sensor").child("Temperature");

        databaseReference_temp.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {

                if(snapshot.getValue()==null){
//                    button_createservice.setVisibility(View.VISIBLE);
//                    recyclerView_serviceView.setVisibility(View.GONE);
                }
                else {

                    String temp = snapshot.getValue().toString();



                    //  temperaturevalue.setText();
                    temperaturevalue.setText(temp+"\u00B0"+"c");
                    progressBar.setVisibility(View.GONE);
                    linearLayout.setVisibility(View.VISIBLE);




                }



            }





            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });


        databaseReference_light = FirebaseDatabase.getInstance().getReference().child("sensor").child("Ldr");

        databaseReference_light.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {

                if(snapshot.getValue()==null){
//                    button_createservice.setVisibility(View.VISIBLE);
//                    recyclerView_serviceView.setVisibility(View.GONE);
                }
                else {

                    String light = snapshot.getValue().toString();
                    int i = Integer. parseInt(light);

                    value = (i-8000)*(100-0)/(100-8000)+0;
                    float per = value*100;




                  // (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;


                    //  temperaturevalue.setText();
                    lightvalue.setText(value+" %");



                }



            }



            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });



        databaseReference_color = FirebaseDatabase.getInstance().getReference().child("sensor").child("Color");

        databaseReference_color.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {

                if(snapshot.getValue()==null){
//                    button_createservice.setVisibility(View.VISIBLE);
//                    recyclerView_serviceView.setVisibility(View.GONE);
                }
                else {

                    String colorsvalue = snapshot.getValue().toString();
                    progressBar.setVisibility(View.GONE);
                    linearLayout.setVisibility(View.VISIBLE);

                    //  temperaturevalue.setText();

                    if (colorsvalue.equals("1")){
                        colorvalue.setText("Growing");
                    }
                    else {
                        colorvalue.setText("Ready to Harvest");
                    }




                }



            }



            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

    }

    private void updatebarlevel(){

      // progressBaranimator = findViewById(R.id.barlevel);
   //    objectAnimator = ObjectAnimator.ofInt(progressBaranimator, "progress",0,100);
//        progressBaranimator.setMax(50);
//        progressBaranimator.setScaleY(2f);
//
//
//        ProgressAnimation progressAnimation = new ProgressAnimation(this,textViewnumber,progressBaranimator,0f,25f);
//        progressBaranimator.setAnimation(progressAnimation);

    }

    }





