package com.manny.piapp.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.manny.piapp.Adapter.StudentList_Adapter;
import com.manny.piapp.R;

import java.util.ArrayList;

public class Studentlist_Activtiy extends AppCompatActivity {

    private RecyclerView recyclerView_studentlist;
    private ArrayList<String> stringArrayList;
    private StudentList_Adapter studentList_adapter;

    private DatabaseReference databaseReference;
    private ProgressBar progressBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_studentlist__activtiy);

        recyclerView_studentlist = findViewById(R.id.attendance_recycleview);
        progressBar = findViewById(R.id.list_progressbar);
        progressBar.setVisibility(View.VISIBLE);
        stringArrayList = new ArrayList<>();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView_studentlist.setLayoutManager(linearLayoutManager);

        databaseReference  = FirebaseDatabase.getInstance().getReference("carduser");


        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                stringArrayList.clear();
                if(snapshot==null){

                }
                else{
                    progressBar.setVisibility(View.GONE);
                    for(DataSnapshot post:snapshot.getChildren()){
                     String name = post.getValue(String.class);

                    stringArrayList.add(name);

                    }

                     studentList_adapter = new StudentList_Adapter(Studentlist_Activtiy.this,stringArrayList);
                    recyclerView_studentlist.setAdapter(studentList_adapter);

                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }
}
