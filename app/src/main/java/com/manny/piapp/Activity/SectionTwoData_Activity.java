package com.manny.piapp.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.manny.piapp.Adapter.SectionListData_Adapter;
import com.manny.piapp.Model.SectionModel;
import com.manny.piapp.R;

import java.util.ArrayList;
import java.util.List;

public class SectionTwoData_Activity extends AppCompatActivity {

    private RecyclerView recyclerView_secondsection;
    private SectionListData_Adapter sectionListData_adapter;
    private List<SectionModel> sectionModelList;
    private DatabaseReference databaseReference_second;
    private ProgressBar progressBar;
    private ImageView imageView_nodataB;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_section_two_data_);


        recyclerView_secondsection = findViewById(R.id.sectionB_recycleview);
        progressBar = findViewById(R.id.sectionA_progressbar);
        imageView_nodataB = findViewById(R.id.nodata_sectionBImage);
        progressBar.setVisibility(View.VISIBLE);

        sectionModelList = new ArrayList<>();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView_secondsection.setLayoutManager(linearLayoutManager);

        databaseReference_second = FirebaseDatabase.getInstance().getReference("Section Two");
        databaseReference_second.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                sectionModelList.clear();

                if(snapshot==null){
                    Toast.makeText(SectionTwoData_Activity.this, "No Data", Toast.LENGTH_SHORT).show();
                    imageView_nodataB.setVisibility(View.VISIBLE);
                }
                else {

                    progressBar.setVisibility(View.GONE);
                    for (DataSnapshot post:snapshot.getChildren()){
                        SectionModel model = post.getValue(SectionModel.class);
                        sectionModelList.add(model);
                    }
                    sectionListData_adapter = new SectionListData_Adapter(SectionTwoData_Activity.this,sectionModelList);
                    recyclerView_secondsection.setAdapter(sectionListData_adapter);
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

    }
}
