package com.manny.piapp.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.manny.piapp.Adapter.SectionListData_Adapter;
import com.manny.piapp.Model.SectionModel;
import com.manny.piapp.R;

import java.util.ArrayList;
import java.util.List;

public class SectionAData_Activity extends AppCompatActivity {

    private RecyclerView recyclerView_section1;
    private SectionListData_Adapter sectionListData_adapter;
    private List<SectionModel> sectionModelList;

    private DatabaseReference databaseReference;
    private ImageView imageView_nodata;
    private ProgressBar progressBar;



    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_section_adata_);


        recyclerView_section1 = findViewById(R.id.sectionA_recycleview);
        imageView_nodata = findViewById(R.id.nodata_sectionAImage);
        progressBar = findViewById(R.id.sectionB_progressbar);
        progressBar.setVisibility(View.VISIBLE);

        sectionModelList = new ArrayList<>();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView_section1.setLayoutManager(linearLayoutManager);

        databaseReference = FirebaseDatabase.getInstance().getReference("Section One");

        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                sectionModelList.clear();

                if(snapshot==null){
                    Toast.makeText(SectionAData_Activity.this, "No Data", Toast.LENGTH_SHORT).show();
                    imageView_nodata.setVisibility(View.VISIBLE);
                }
                else {
                    progressBar.setVisibility(View.GONE);
                    for (DataSnapshot post:snapshot.getChildren()){
                        SectionModel model = post.getValue(SectionModel.class);
                        sectionModelList.add(model);
                    }
                    sectionListData_adapter = new SectionListData_Adapter(SectionAData_Activity.this,sectionModelList);
                    recyclerView_section1.setAdapter(sectionListData_adapter);
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }
}
