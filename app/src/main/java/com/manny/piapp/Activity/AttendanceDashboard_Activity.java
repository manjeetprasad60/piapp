package com.manny.piapp.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.manny.piapp.Model.SectionModel;
import com.manny.piapp.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

public class AttendanceDashboard_Activity extends AppCompatActivity {

    private CardView cardView_studentlist;
    private CardView cardView_sectionA;
    private CardView cardView_sectionB;
    private Switch swA,swB;
    private DatabaseReference reference_sectionA, reference_sectiionB,reference_selection;
    private String idA,idB;
    private String date;
    private TextView textView_data;

    private ImageView imageView_sidemenu;
    private DatabaseReference databaseReference_sectionARemove,databaseReference_sectionBRemove,databaseReference_deletedsection;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendance_dashboard_);


        cardView_studentlist = findViewById(R.id.studentlist);
        cardView_sectionA = findViewById(R.id.sectionAcardview);
        cardView_sectionB = findViewById(R.id.sectiontwo_cardview);
        swA = findViewById(R.id.switchSectionA);
        swB = findViewById(R.id.switchSectionB);
        textView_data = findViewById(R.id.currentdata_tv);
        imageView_sidemenu = findViewById(R.id.sidemenuoption);



        imageView_sidemenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PopupMenu popupMenu = new PopupMenu(AttendanceDashboard_Activity.this,imageView_sidemenu);
                popupMenu.inflate(R.menu.sidemenu);

                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()){
                            case R.id.cleardata:
                                Toast.makeText(AttendanceDashboard_Activity.this,
                                        "Attendance Data clear", Toast.LENGTH_SHORT).show();
                                databaseReference_deletedsection = FirebaseDatabase.getInstance().getReference("delete");
                                databaseReference_sectionBRemove = FirebaseDatabase.getInstance().getReference("Section Two");
                                databaseReference_sectionARemove = FirebaseDatabase.getInstance().getReference("Section One");
                                databaseReference_deletedsection.removeValue();
                                databaseReference_sectionARemove.removeValue();
                                databaseReference_sectionBRemove.removeValue();
                                break;
                                default:
                                    break;
                        }
                        return false;
                    }
                });
                popupMenu.show();

            }
        });



        //Calculating date
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy");
        date = simpleDateFormat.format(calendar.getTime());
        textView_data.setText("Date:- "+date);

        swA.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    counttime();
                    // The toggle is enabled
                    sectionAselecton();
                    swB.setChecked(false);
                } else {
                    // The toggle is disabled

                }
            }
        });


        swB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    // The toggle is enabled
                    counttime();
                    sectionBselecton();
                    swA.setChecked(false);
                } else {
                    // The toggle is disabled

                }
            }
        });


        cardView_studentlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(AttendanceDashboard_Activity.this,Studentlist_Activtiy.class);
                startActivity(i);

            }
        });
        cardView_sectionA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent j = new Intent(AttendanceDashboard_Activity.this,SectionAData_Activity.class);
                startActivity(j);

            }
        });
        cardView_sectionB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent k = new Intent(AttendanceDashboard_Activity.this,SectionTwoData_Activity.class);
                startActivity(k);

            }
        });
    }

    private void sectionAselecton() {

        DatabaseReference databaseReference_selection = FirebaseDatabase.getInstance().getReference("selection section");

        databaseReference_selection.child("section").setValue("A");


    }

    private void sectionBselecton() {

        DatabaseReference databaseReference_selection = FirebaseDatabase.getInstance().getReference("selection section");

        databaseReference_selection.child("section").setValue("B");




    }

    private void sidemenu(){

    }
    private void counttime(){





                DatabaseReference databaseReference_late = FirebaseDatabase.getInstance().getReference("Timing");

                SectionModel sectionModel = new SectionModel();

                sectionModel.setLate("OnTime");

                databaseReference_late.setValue(sectionModel);

        try {
            TimeUnit.MINUTES.sleep(15);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        DatabaseReference databaseReference_t = FirebaseDatabase.getInstance().getReference("Timing");

                SectionModel sectionModel2 = new SectionModel();

                sectionModel.setLate("Late");

                databaseReference_late.setValue(sectionModel);




    }



}
