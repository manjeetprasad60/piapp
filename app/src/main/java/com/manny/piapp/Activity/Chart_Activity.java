package com.manny.piapp.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.manny.piapp.R;

import java.util.ArrayList;

public class Chart_Activity extends AppCompatActivity {

    LineChart lineChart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chart_);

        lineChart = findViewById(R.id.line_chart);


            LineDataSet lineDataSet = new LineDataSet(datavalue1(),"Data set 1");
            ArrayList<ILineDataSet> dataSets = new ArrayList<>();
            dataSets.add(lineDataSet);

            LineData  lineData = new LineData((dataSets));
            lineChart.setData(lineData);
            lineChart.invalidate();



    }

    private ArrayList<Entry> datavalue1(){

        ArrayList<Entry> dataValus = new ArrayList<Entry>();
        dataValus.add(new Entry(0,20));
        dataValus.add(new Entry(1,24));
        dataValus.add(new Entry(2,28));
        dataValus.add(new Entry(3,35));
        dataValus.add(new Entry(4,33));
        dataValus.add(new Entry(5,24));

         return  dataValus;
    }
}
