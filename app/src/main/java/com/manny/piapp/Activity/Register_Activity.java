
package com.manny.piapp.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.manny.piapp.R;

import static android.icu.lang.UCharacter.GraphemeClusterBreak.V;

public class Register_Activity extends AppCompatActivity {

    private TextView  textView_LoginHere;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_);
        textView_LoginHere = findViewById(R.id.alreadyaccount);

        textView_LoginHere.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i =  new Intent(Register_Activity.this,Login_Activity.class);
                startActivity(i);
            }
        });
    }
}
