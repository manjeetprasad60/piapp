package com.manny.piapp.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.manny.piapp.R;

public class Login_Activity extends AppCompatActivity {
    private TextView textView_createaccount;
    private Button button_login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_);
        textView_createaccount = findViewById(R.id.createaccount_tv);
        button_login = findViewById(R.id.loginbtn);

        textView_createaccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent j = new Intent(Login_Activity.this,Register_Activity.class);
                startActivity(j);
            }
        });

        button_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i= new Intent(Login_Activity.this,BarLevelindicator_activity.class);
                startActivity(i);
            }
        });
    }
}
