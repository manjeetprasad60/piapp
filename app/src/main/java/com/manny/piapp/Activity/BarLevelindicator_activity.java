package com.manny.piapp.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.manny.piapp.R;

public class BarLevelindicator_activity extends AppCompatActivity {

    private ImageView zero,ten,twenty,thirty,fourty,fifity,sixty,seventy,eighty,ninty,hundredth;
    private  int tempint = 100;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bar_levelindicator_activity);



        zero = findViewById(R.id.zero);
        ten = findViewById(R.id.ten);
        twenty = findViewById(R.id.twnety);
        thirty = findViewById(R.id.thirty);
        fourty = findViewById(R.id.fourty);
        fifity = findViewById(R.id.fifty);
        sixty = findViewById(R.id.sixty);
        seventy = findViewById(R.id.seventy);
        eighty = findViewById(R.id.eightty);
        ninty = findViewById(R.id.nighty);
        hundredth = findViewById(R.id.hundred);




        if(tempint<=5){
            ten.setVisibility(View.VISIBLE);
            zero.setVisibility(View.VISIBLE);
            twenty.setVisibility(View.GONE);
            thirty.setVisibility(View.GONE);
            fourty.setVisibility(View.GONE);
            fifity.setVisibility(View.GONE);
            sixty.setVisibility(View.GONE);
            seventy.setVisibility(View.GONE);
            eighty.setVisibility(View.GONE);
            ninty.setVisibility(View.GONE);
           // humidityvalue.setVisibility(View.GONE);

        }
        if(tempint>5&&tempint<=10){
            ten.setVisibility(View.GONE);
            zero.setVisibility(View.VISIBLE);
            twenty.setVisibility(View.VISIBLE);
            thirty.setVisibility(View.GONE);
            fourty.setVisibility(View.GONE);
            fifity.setVisibility(View.GONE);
            sixty.setVisibility(View.GONE);
            seventy.setVisibility(View.GONE);
            eighty.setVisibility(View.GONE);
            ninty.setVisibility(View.GONE);
            //humidityvalue.setVisibility(View.GONE);

        }
        if(tempint>10&&tempint<=15){
            ten.setVisibility(View.GONE);
            zero.setVisibility(View.VISIBLE);
            twenty.setVisibility(View.GONE);
            thirty.setVisibility(View.VISIBLE);
            fourty.setVisibility(View.GONE);
            fifity.setVisibility(View.GONE);
            sixty.setVisibility(View.GONE);
            seventy.setVisibility(View.GONE);
            eighty.setVisibility(View.GONE);
            ninty.setVisibility(View.GONE);
            //humidityvalue.setVisibility(View.GONE);

        }
        if(tempint>15&&tempint<=20){
            ten.setVisibility(View.GONE);
            zero.setVisibility(View.VISIBLE);
            twenty.setVisibility(View.GONE);
            thirty.setVisibility(View.GONE);
            fourty.setVisibility(View.VISIBLE);
            fifity.setVisibility(View.GONE);
            sixty.setVisibility(View.GONE);
            seventy.setVisibility(View.GONE);
            eighty.setVisibility(View.GONE);
            ninty.setVisibility(View.GONE);
            //humidityvalue.setVisibility(View.GONE);

        }
        if(tempint>20&&tempint<=25){
            ten.setVisibility(View.GONE);
            zero.setVisibility(View.VISIBLE);
            twenty.setVisibility(View.GONE);
            thirty.setVisibility(View.GONE);
            fourty.setVisibility(View.GONE);
            fifity.setVisibility(View.VISIBLE);
            sixty.setVisibility(View.GONE);
            seventy.setVisibility(View.GONE);
            eighty.setVisibility(View.GONE);
            ninty.setVisibility(View.GONE);
            //humidityvalue.setVisibility(View.GONE);

        }

        if(tempint>25&&tempint<=30){
            ten.setVisibility(View.GONE);
            zero.setVisibility(View.VISIBLE);
            twenty.setVisibility(View.GONE);
            thirty.setVisibility(View.GONE);
            fourty.setVisibility(View.GONE);
            fifity.setVisibility(View.GONE);
            sixty.setVisibility(View.VISIBLE);
            seventy.setVisibility(View.GONE);
            eighty.setVisibility(View.GONE);
            ninty.setVisibility(View.GONE);
            //humidityvalue.setVisibility(View.GONE);

        }
        if(tempint>25&&tempint<=30){
            ten.setVisibility(View.GONE);
            zero.setVisibility(View.VISIBLE);
            twenty.setVisibility(View.GONE);
            thirty.setVisibility(View.GONE);
            fourty.setVisibility(View.GONE);
            fifity.setVisibility(View.GONE);
            sixty.setVisibility(View.VISIBLE);
            seventy.setVisibility(View.GONE);
            eighty.setVisibility(View.GONE);
            ninty.setVisibility(View.GONE);
            //humidityvalue.setVisibility(View.GONE);

        }
        if(tempint>30&&tempint<=35){
            ten.setVisibility(View.GONE);
            zero.setVisibility(View.VISIBLE);
            twenty.setVisibility(View.GONE);
            thirty.setVisibility(View.GONE);
            fourty.setVisibility(View.GONE);
            fifity.setVisibility(View.GONE);
            sixty.setVisibility(View.GONE);
            seventy.setVisibility(View.VISIBLE);
            eighty.setVisibility(View.GONE);
            ninty.setVisibility(View.GONE);
            //humidityvalue.setVisibility(View.GONE);

        }
        if(tempint>35&&tempint<=40){
            ten.setVisibility(View.GONE);
            zero.setVisibility(View.VISIBLE);
            twenty.setVisibility(View.GONE);
            thirty.setVisibility(View.GONE);
            fourty.setVisibility(View.GONE);
            fifity.setVisibility(View.GONE);
            sixty.setVisibility(View.GONE);
            seventy.setVisibility(View.GONE);
            eighty.setVisibility(View.VISIBLE);
            ninty.setVisibility(View.GONE);
            //humidityvalue.setVisibility(View.GONE);

        }
        if(tempint>40&&tempint<=45){
            ten.setVisibility(View.GONE);
            zero.setVisibility(View.VISIBLE);
            twenty.setVisibility(View.GONE);
            thirty.setVisibility(View.GONE);
            fourty.setVisibility(View.GONE);
            fifity.setVisibility(View.GONE);
            sixty.setVisibility(View.GONE);
            seventy.setVisibility(View.GONE);
            eighty.setVisibility(View.GONE);
            ninty.setVisibility(View.VISIBLE);
            //humidityvalue.setVisibility(View.GONE);

        }
        if(tempint>45&&tempint<=50){
            ten.setVisibility(View.GONE);
            zero.setVisibility(View.VISIBLE);
            twenty.setVisibility(View.GONE);
            thirty.setVisibility(View.GONE);
            fourty.setVisibility(View.GONE);
            fifity.setVisibility(View.GONE);
            sixty.setVisibility(View.GONE);
            seventy.setVisibility(View.GONE);
            eighty.setVisibility(View.GONE);
            ninty.setVisibility(View.GONE);
            //humidityvalue.setVisibility(View.VISIBLE);

        }

    }
}
