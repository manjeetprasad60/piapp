package com.manny.piapp.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.os.Bundle;
import android.widget.CompoundButton;
import android.widget.ToggleButton;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.manny.piapp.R;

public class HomeAutomation extends AppCompatActivity {

    private ToggleButton toggleButton_r1;
    private ToggleButton toggleButton_r2;
    private ToggleButton toggleButton_r3;
    private ToggleButton toggleButton_r4;
    private ToggleButton toggleButton_r5;
    private ToggleButton toggleButton_r6;
    private ToggleButton toggleButton_r7;
    private ToggleButton toggleButton_r8;
    private ToggleButton toggleButton_r9;
    private ToggleButton toggleButton_r10;
    private ToggleButton toggleButton_all;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_automation);

        toggleButton_r1 = findViewById(R.id.relay_one);
        toggleButton_r2 = findViewById(R.id.relay_two);
        toggleButton_r3 = findViewById(R.id.relay_three);
        toggleButton_r4 = findViewById(R.id.relay_four);
        toggleButton_r5 = findViewById(R.id.relay_five);
        toggleButton_r6 = findViewById(R.id.relay_six);
        toggleButton_r7 = findViewById(R.id.relay_seven);
        toggleButton_r8 = findViewById(R.id.relay_eight);
        toggleButton_r9 = findViewById(R.id.relay_nine);
        toggleButton_r10 = findViewById(R.id.relay_ten);
        toggleButton_all = findViewById(R.id.relay_all);

        toggleButton_r1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(toggleButton_r1.isChecked()){

                    DatabaseReference databaseReference_r1 = FirebaseDatabase.getInstance().getReference("NodeMcu");

                    databaseReference_r1.child("SwitchOne").child("Relay").setValue("A");

                }
                else{
                    DatabaseReference databaseReference_r1 = FirebaseDatabase.getInstance().getReference("NodeMcu");

                    databaseReference_r1.child("SwitchOne").child("Relay").setValue("AB");
                }
            }
        });

        toggleButton_r2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(toggleButton_r2.isChecked()){

                    DatabaseReference databaseReference_r1 = FirebaseDatabase.getInstance().getReference("NodeMcu");

                    databaseReference_r1.child("SwitchTwo").child("Relay").setValue("B");

                }
                else{
                    DatabaseReference databaseReference_r1 = FirebaseDatabase.getInstance().getReference("NodeMcu");

                    databaseReference_r1.child("SwitchTwo").child("Relay").setValue("BB");
                }
            }
        });

        toggleButton_r3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(toggleButton_r3.isChecked()){

                    DatabaseReference databaseReference_r1 = FirebaseDatabase.getInstance().getReference("NodeMcu");

                    databaseReference_r1.child("SwitchThree").child("Relay").setValue("C");

                }
                else{
                    DatabaseReference databaseReference_r1 = FirebaseDatabase.getInstance().getReference("NodeMcu");

                    databaseReference_r1.child("SwitchThree").child("Relay").setValue("CB");
                }
            }
        });
        toggleButton_r4.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(toggleButton_r4.isChecked()){

                    DatabaseReference databaseReference_r1 = FirebaseDatabase.getInstance().getReference("NodeMcu");

                    databaseReference_r1.child("SwitchFour").child("Relay").setValue("D");

                }
                else{
                    DatabaseReference databaseReference_r1 = FirebaseDatabase.getInstance().getReference("NodeMcu");

                    databaseReference_r1.child("SwitchFour").child("Relay").setValue("DB");
                }
            }
        });

        toggleButton_r5.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(toggleButton_r5.isChecked()){

                    DatabaseReference databaseReference_r1 = FirebaseDatabase.getInstance().getReference("NodeMcu");

                    databaseReference_r1.child("SwitchFive").child("Relay").setValue("E");
                }
                else{
                    DatabaseReference databaseReference_r1 = FirebaseDatabase.getInstance().getReference("NodeMcu");

                    databaseReference_r1.child("SwitchFive").child("Relay").setValue("EB");
                }
            }
        });
        toggleButton_r6.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(toggleButton_r6.isChecked()){

                    DatabaseReference databaseReference_r1 = FirebaseDatabase.getInstance().getReference("NodeMcu");

                    databaseReference_r1.child("SwitchSix").child("Relay").setValue("F");
                }
                else{
                    DatabaseReference databaseReference_r1 = FirebaseDatabase.getInstance().getReference("NodeMcu");

                    databaseReference_r1.child("SwitchSix").child("Relay").setValue("FB");
                }
            }
        });
        toggleButton_r7.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(toggleButton_r7.isChecked()){

                    DatabaseReference databaseReference_r1 = FirebaseDatabase.getInstance().getReference("NodeMcu");

                    databaseReference_r1.child("SwitchSeven").child("Relay").setValue("G");

                }
                else{
                    DatabaseReference databaseReference_r1 = FirebaseDatabase.getInstance().getReference("NodeMcu");

                    databaseReference_r1.child("SwitchSeven").child("Relay").setValue("GB");
                }
            }
        });
        toggleButton_r8.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(toggleButton_r8.isChecked()){

                    DatabaseReference databaseReference_r1 = FirebaseDatabase.getInstance().getReference("NodeMcu");

                    databaseReference_r1.child("SwitchEight").child("Relay").setValue("H");

                }
                else{
                    DatabaseReference databaseReference_r1 = FirebaseDatabase.getInstance().getReference("NodeMcu");

                    databaseReference_r1.child("SwitchEight").child("Relay").setValue("HB");
                }
            }
        });
        toggleButton_r9.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(toggleButton_r9.isChecked()){

                    DatabaseReference databaseReference_r1 = FirebaseDatabase.getInstance().getReference("NodeMcu");

                    databaseReference_r1.child("SwitchNine").child("Relay").setValue("I");

                }
                else{
                    DatabaseReference databaseReference_r1 = FirebaseDatabase.getInstance().getReference("NodeMcu");

                    databaseReference_r1.child("SwitchNine").child("Relay").setValue("IB");
                }
            }
        });
        toggleButton_r10.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(toggleButton_r10.isChecked()){

                    DatabaseReference databaseReference_r1 = FirebaseDatabase.getInstance().getReference("NodeMcu");

                    databaseReference_r1.child("SwitchTen").child("Relay").setValue("J");

                }
                else{
                    DatabaseReference databaseReference_r1 = FirebaseDatabase.getInstance().getReference("NodeMcu");

                    databaseReference_r1.child("SwitchTen").child("Relay").setValue("JB");
                }
            }
        });
        toggleButton_all.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(toggleButton_all.isChecked()){

                    DatabaseReference databaseReference_r1 = FirebaseDatabase.getInstance().getReference("NodeMcu");

                    databaseReference_r1.child("SwitchAll").child("Relay").setValue("K");

                }
                else{
                    DatabaseReference databaseReference_r1 = FirebaseDatabase.getInstance().getReference("NodeMcu");

                    databaseReference_r1.child("SwitchAll").child("Relay").setValue("KB");
                }
            }
        });





    }
}
