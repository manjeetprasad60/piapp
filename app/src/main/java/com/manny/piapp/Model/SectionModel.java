package com.manny.piapp.Model;

public class SectionModel {
  private long id;
  private String name;
  private String time;
  private String late;
  private String status;

    public SectionModel(long id, String name, String time, String late,String status) {
        this.id = id;
        this.name = name;
        this.time = time;
        this.late = late;
        this.status = status;
    }

    public SectionModel() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getLate() {
        return late;
    }

    public void setLate(String late) {
        this.late = late;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
