package com.manny.piapp.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.manny.piapp.Model.SectionModel;
import com.manny.piapp.R;

import java.util.ArrayList;
import java.util.List;

public class SectionListData_Adapter extends RecyclerView.Adapter<SectionListData_Adapter.ViewHolder> {

    private Context context;
    private List<SectionModel> sectionModelList;



    public SectionListData_Adapter(Context context, List<SectionModel> sectionModelList) {
        this.context = context;
        this.sectionModelList = sectionModelList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.section_item,parent,false);

        return  new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        SectionModel sectionModel = sectionModelList.get(position);

        holder.textView_id.setText("Card ID:- " + sectionModel.getId()+"");
        holder.textView_name.setText(sectionModel.getName());
        holder.textView_time.setText("Date:- "+ sectionModel.getTime());
        holder.textView_status.setText(sectionModel.getStatus());





    }
//    private void campere(){
//        reference_sectionA = FirebaseDatabase.getInstance().getReference("Section One");
//        reference_sectionA.addValueEventListener(new ValueEventListener() {
//            @Override
//
//            public void onDataChange(@NonNull DataSnapshot snapshot) {
//
//                if(snapshot==null){
//
//                }
//                else {
//                    for (DataSnapshot post:snapshot.getChildren()){
//                        SectionModel model = post.getValue(SectionModel.class);
//                        modelList.add(model);
//
//                        for (int i = 0; i<=modelList.size(); i++){
//
//                        }
//                    }
//                }
//
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError error) {
//
//            }
//        });
//
//        reference_selection = FirebaseDatabase.getInstance().getReference();
//
//        reference_selection.child("selection section").child("section").addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot snapshot) {
//                if(snapshot==null){
//
//                }
//                else {
//                    String selection = snapshot.getValue().toString();
//
//                    if(selection=="A"){
//
//                    }
//                }
//
//
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError error) {
//
//            }
//        });
//    }

    @Override
    public int getItemCount() {
        return sectionModelList!=null? sectionModelList.size():0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private TextView textView_id,textView_name,textView_time,textView_status;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            textView_id =  itemView.findViewById(R.id.idtext);
            textView_name = itemView.findViewById(R.id.nametext);
            textView_time = itemView.findViewById(R.id.timetext);
            textView_status = itemView.findViewById(R.id.statustext);
        }
    }

}
