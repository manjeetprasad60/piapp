package com.manny.piapp.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.manny.piapp.R;

import java.util.ArrayList;

public class StudentList_Adapter extends RecyclerView.Adapter<StudentList_Adapter.MyViewHold> {

    private Context context;
    private ArrayList<String> arrayList;

    public StudentList_Adapter(Context context, ArrayList<String> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }


    @NonNull
    @Override
    public MyViewHold onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
       View view = LayoutInflater.from(context).inflate(R.layout.student_item,parent,false);

       return new MyViewHold(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHold holder, int position) {

        holder.textView.setText(arrayList.get(position));

    }

    @Override
    public int getItemCount() {
        return arrayList!=null? arrayList.size():0;

    }

    public class MyViewHold extends RecyclerView.ViewHolder{

        private TextView textView;

        public MyViewHold(@NonNull View itemView) {
            super(itemView);

            textView = itemView.findViewById(R.id.studentname);
        }
    }
}
