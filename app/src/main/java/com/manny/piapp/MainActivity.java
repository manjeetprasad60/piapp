package com.manny.piapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.manny.piapp.Activity.AttendanceDashboard_Activity;
import com.manny.piapp.Activity.BarLevelindicator_activity;
import com.manny.piapp.Activity.Chart_Activity;
import com.manny.piapp.Activity.HomeAutomation;
import com.manny.piapp.Activity.Login_Activity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(MainActivity.this, HomeAutomation

                        .class));
                finish();
            }
        },3000);
    }
}
